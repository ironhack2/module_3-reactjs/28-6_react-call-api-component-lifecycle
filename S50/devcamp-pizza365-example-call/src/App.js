import "bootstrap/dist/css/bootstrap.min.css";
import FetchAPI from "./components/FetchAPI";
import AxiosLibrary from "./components/AxiosLibrary";

function App() {
  return (
    <div className="container">
     <FetchAPI />
      <AxiosLibrary />
    </div>
  );
}

export default App;
