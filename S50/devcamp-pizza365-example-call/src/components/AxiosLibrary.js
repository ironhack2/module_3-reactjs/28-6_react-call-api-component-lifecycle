import { Component } from "react";

import axios from "axios";


class AxiosLibrary extends Component {

    axiosLibrary = async (url, body) => {
        let response = await axios(url, body);

        return response.data
    }

    getAllAPIOrder = () => {
        console.log("Axios Get All API Order");

        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);
            })
    }


    postAPIOrder = () => {
        console.log("Axios Post  API Order");

        let body = {
            kichCo: "M",
            duongKinh: "25",
            suon: "4",
            salad: "300",
            loaiPizza: "HAWAII",
            idVourcher: "16512",
            idLoaiNuocUong: "PEPSI",
            soLuongNuoc: "3",
            hoTen: "Phạm Thanh Bình",
            thanhTien: "200000",
            email: "binhpt001@devcamp.edu.vn",
            soDienThoai: "0865241654",
            diaChi: "Hà Nội",
            loiNhan: "Pizza đế dày"
        };

        axios.post("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data.data);
            })
    }


    getByIdAPIOrder = () => {
        console.log("Axios Get By ID API Order");

        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/orders/Y6BIbCsDpz")
            .then((data) => {
                console.log(data);
            })
    }

    updateByIdAPI = () => {
        console.log("Axios get API Voucher By ID");

        let body = {
            trangThai: "confirmed",
        };

        axios.put("http://42.115.221.44:8080/devcamp-pizza365/orders/438481", body)
        .then((data)=>{
            console.log(data.data);
        })
    }


    checkVoucherById = () =>{
        console.log("Axios get All API Drink");

        this.axiosLibrary("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/12332")
        .then((data)=>{
            console.log(data);
        })
    }


    getAllAPIDrink = () =>{
        console.log("Axios Put  API Order");

        this.axiosLibrary("http://42.115.221.44:8080/devcamp-pizza365/drinks")
        .then((data)=>{
            console.log(data);
        })
    }

    render() {
        return (
            <div className="row mt-5">
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.getAllAPIOrder}>Call api get all orders!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-info" onClick={this.postAPIOrder}>Call api create order!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-success" onClick={this.getByIdAPIOrder}>Call api get order by id!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-secondary" onClick={this.updateByIdAPI}>Call api update order!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-danger" onClick={this.checkVoucherById}>Call api check voucher by id!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-success" onClick={this.getAllAPIDrink}>Call api Get drink list</button>
                </div>
            </div>
        )
    }
}

export default AxiosLibrary;