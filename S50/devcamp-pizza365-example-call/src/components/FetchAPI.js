import { Component } from "react";

class FetchAPI extends Component {

    fetchAPI = async (url, body) => {
        let response = await fetch(url, body);
        let data = await response.json();

        return data;
    }

    getAllAPIOrder = () => {
        console.log("Fetch Get All API Order");

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);
            })
    }

    getByIdAPIOrder = () => {
        console.log("Fetch Get By ID API Order");

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/Y6BIbCsDpz")
            .then((data) => {
                console.log(data);
            })
    }

    postAPIOrder = () => {
        console.log("Fetch Post  API Order");

        let body = {
            method: "POST",
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
            },
        };

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data);
            })
    }


    updateByIdAPI = () => {
        console.log("Fetch Put  API Order");

        let body = {
            method: 'PUT',
            body: JSON.stringify({
                trangThai: "confirmed",
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders/438460", body)
            .then((data) => {
                console.log(data);
            })
    }

    checkVoucherById = () => {
        console.log("Fetch get API Voucher By ID");

        this.fetchAPI("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/12332")
            .then((data) => {
                console.log(data);
            })
    }

    getAllAPIDrink = () =>{
        console.log("Fetch get All API Drink");

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/drinks")
        .then((data) => {
            console.log(data);
        })
    }

    render() {
        return (
            <div className="row mt-5">
                <div className="col-2">
                    <button className="btn btn-primary" onClick={this.getAllAPIOrder}>Call api get all orders!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-info" onClick={this.postAPIOrder}>Call api create order!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-success" onClick={this.getByIdAPIOrder}>Call api get order by id!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-secondary" onClick={this.updateByIdAPI}>Call api update order!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-danger" onClick={this.checkVoucherById}>Call api check voucher by id!</button>
                </div>

                <div className="col-2">
                    <button className="btn btn-success" onClick={this.getAllAPIDrink}>Call api Get drink list</button>
                </div>
            </div>
        )
    }
}

export default FetchAPI;