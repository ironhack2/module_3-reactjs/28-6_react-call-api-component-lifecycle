import React, { Component } from 'react';

import Display from "./Display";

class Parent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: false,
            colorBtn: "btn btn-info",
            textBtn: "Create Component",
        }
    }

    buttonUpdate = () => {
        if(this.state.display) {
            this.setState({
                display: false,
                colorBtn: "btn btn-info",
                textBtn: "Create Component",
            })
        } else {
            this.setState({
                display: true,
                colorBtn: "btn btn-danger",
                textBtn: "Destroy Component",
            })
        }
    }

    render() {
        return (
            <div className="container mt-5">
                <button className={this.state.colorBtn} onClick={this.buttonUpdate}>{this.state.textBtn}</button>

                {this.state.display ? <Display buttonUpdate={this.updateDisplay} /> : null}
            </div>
        )
    }
}

export default Parent