import { Component } from "react";

class Display extends Component {


    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
    }

    componentWillUnmount() {
        console.log("Component will unmount");
    }

    render() {
        return (
            <div className="container mt-5">
                    <h1>I exist !</h1>
            </div>
        )
    }
}

export default Display