import Parent from './components/Parent';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div>
      <Parent />
    </div>
  );
}

export default App;
