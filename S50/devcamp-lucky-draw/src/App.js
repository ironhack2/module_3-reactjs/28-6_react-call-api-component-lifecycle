import './App.css';
import "bootstrap/dist/css/bootstrap.min.css"
import LuckyDraw from "./components/LuckyDraw"

function App() {
  return (
    <div className="container mt-5 text-center">
      <LuckyDraw />
    </div>
  );
}

export default App;
