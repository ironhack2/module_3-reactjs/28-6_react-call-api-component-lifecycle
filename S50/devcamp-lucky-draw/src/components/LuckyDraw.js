import { Component } from 'react';

class LuckyDraw extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ball1: 1,
            ball2: 2,
            ball3: 3,
            ball4: 4,
            ball5: 5,
            ball6: 6,
        }
    }

    buttonGenerate = () =>{
        console.log("Button Generate Click")
        this.setState({
            ball1: Math.floor((Math.random() * 99) + 1),
            ball2: Math.floor((Math.random() * 99) + 1),
            ball3: Math.floor((Math.random() * 99) + 1),
            ball4: Math.floor((Math.random() * 99) + 1),
            ball5: Math.floor((Math.random() * 99) + 1),
            ball6: Math.floor((Math.random() * 99) + 1),
        })
    }


    render() {
        return (
            <div>
                <h1>Lucky Draw</h1>

                <div className="d-flex align-items-center justify-content-center mt-5">
                    <div className="item-circle d-flex align-items-center justify-content-center">
                        <p>{this.state.ball1}</p>
                    </div>

                    <div className="item-circle d-flex align-items-center justify-content-center">
                        <p>{this.state.ball2}</p>
                    </div>

                    <div className="item-circle d-flex align-items-center justify-content-center">
                        <p>{this.state.ball3}</p>
                    </div>

                    <div className="item-circle d-flex align-items-center justify-content-center">
                        <p>{this.state.ball4}</p>
                    </div>

                    <div className="item-circle d-flex align-items-center justify-content-center">
                        <p>{this.state.ball5}</p>
                    </div>

                    <div className="item-circle d-flex align-items-center justify-content-center">
                        <p>{this.state.ball6}</p>
                    </div>
                </div>

                <button className="btn-color mt-5" onClick={this.buttonGenerate}>Generate</button>
            </div>
        )
    }
}

export default LuckyDraw